﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_KevinDamian_JoshuaValey
{
    class Program
    {
        static void Main(string[] args)
        {


            bool menu = true;

            while (menu)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Ingrese una de las siguientes opciones para continuar" +
                "\n1. Iniciar Programa\n2. Salir");

                switch (Console.ReadLine())
                {
                    case "1":

                        bool avanzar = false;
                        while (avanzar == false)
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            int n = 0;
                            Console.WriteLine("Ingrese un número de discos de 1 a 6");
                            try
                            {
                                n = Convert.ToInt32(Console.ReadLine());
                            }
                            catch (Exception)
                            {

                                
                            }

                            if (0 < n && n <= 6)
                            {
                                Escoger(n);
                                avanzar = true;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Debe escribir un número de 1 a 6");
                                avanzar = false;
                            }
                        }
                        menu = true;
                        break;
                    case "2":
                        menu = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\nNo ingreso una opción correcta, intentelo de nuevo");
                        menu = true;
                        break;
                }
            }



        }

        static void Escoger(int n)
        {
            //int i = 0;
            string[] arrPasos1 =
            {
            "Disco 1 de A a D"
        };
            string[] arrPasos2 =
            {
            "Disco 1 de A a C", "Disco 2 de A a D", "Disco 1 de C A D"
        };
            string[] arrPasos3 =
            {
            "Disco 1 de A a B", "Disco 2 de A a C", "Disco 3 de A a D", "Disco 2 de C a D",
            "Disco 1 de B a C", "Disco 1 de C a D"
        };
            string[] arrPasos4 =
            {
            "Disco 1 de A a C", "Disco 2 de A a B", "Disco 1 de C a B", "Disco 3 de A a C",
            "Disco 4 de A a D", "Disco 3 de C a D", "Disco 1 de B a A", "Disco 2 de B a D",
            "Disco 1 de A a C", "Disco 1 de C a D"
        };
            string[] arrPasos5 =
            {
            "Disco 1 de A a D", "Disco 2 de A a C", "Disco 3 de A a B", "Disco 2 de C a B",
            "Disco 4 de A a C", "Disco 1 de D a B", "Disco 5 de A a D", "Disco 4 de C a D",
            "Disco 1 de B a A", "Disco 2 de B a C", "Disco 3 de B a D", "Disco 2 de C a D",
            "Disco 1 de A a B", "Disco 1 de B a C", "Disco 1 de C a D"
        };
            string[] arrPasos6 =
            {
            "Disco 1 de A a D", "Disco 2 de A a C", "Disco 3 de A a B", "Disco 2 de C a B",
            "Disco 1 de D a B", "Disco 4 de A a D", "Disco 5 de A a C", "Disco 4 de D a C",
            "Disco 6 de A a D", "Disco 4 de C a A", "Disco 5 de C a D", "Disco 4 de A a D",
            "Disco 1 de B a A", "Disco 2 de B a C", "Disco 3 de B a D", "Disco 2 de C a D",
            "Disco 1 de A a B", "Disco 1 de B a C", "Disco 1 de C a B", "Disco 1 de B a C",
            "Disco 1 de C a D"

        };

            if (n == 1)
            {
                Print(arrPasos1);
            }
            if (n == 2)
            {
                Print(arrPasos2);
            }
            if (n == 3)
            {
                Print(arrPasos3);
            }
            if (n == 4)
            {
                Print(arrPasos4);
            }
            if (n == 5)
            {
                Print(arrPasos5);
            }
            if (n == 6)
            {
                Print(arrPasos6);
            }
        }

        static void Print(string[] arr)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (int i = 0; i <= arr.Length - 1; i++)
            {
                Console.WriteLine(arr[i]);

            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Precione una tecla para CONTINUAR");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
